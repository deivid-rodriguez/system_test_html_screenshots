# Changelog

## Unreleased

## 0.2.1

### Fixed

* [#5]: Typo in gem description.

## 0.2.0

### Added

* [#2]: Allow Rails 6.

## 0.1.2

### Fixed

* [#1]: Incorrect requirement that intended to disallow Rails 6 prereleases, but won't work on newer rubygems.

## 0.1.1

### Added

* Monkeypatch `image_path` as well.

## 0.1.0

_Initial version_.

[#1]: https://gitlab.com/deivid-rodriguez/system_test_html_screenshots/merge_requests/1
[#2]: https://gitlab.com/deivid-rodriguez/system_test_html_screenshots/merge_requests/2
[#5]: https://gitlab.com/deivid-rodriguez/system_test_html_screenshots/merge_requests/5

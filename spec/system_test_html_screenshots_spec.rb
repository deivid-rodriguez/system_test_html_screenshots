require "open3"

RSpec.describe SystemTestHtmlScreenshots do
  let(:result) do
    Bundler.with_clean_env do
      Open3.capture2e("cd spec/test_app && bundle install && bin/rails test:system")
    end
  end

  it "works" do
    expect(result[1]).not_to be_success, result[0]
    expect(result[0]).to match(%r{\[Page HTML\]: .*spec/test_app/tmp/screenshots/failures_test_visiting_the_index\.html}), result[0]
  end
end

lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "system_test_html_screenshots/version"

Gem::Specification.new do |spec|
  spec.name          = "system_test_html_screenshots"
  spec.version       = SystemTestHtmlScreenshots::VERSION
  spec.authors       = ["David Rodríguez"]
  spec.email         = ["deivid.rodriguez@riseup.net"]

  spec.summary       = %q{Monkeypatches to Rails system tests so that HTML screenshots are generated.}
  spec.description   = %q{Rails is not interested on adding HTML screenshots to their system tests, so this gem monkeypatches Rails to achieve that.}
  spec.homepage      = "https://gitlab.com/deivid-rodriguez/system_test_html_screenshots"
  spec.license       = "MIT"

  # Specify which files should be added to the gem when it is released.
  spec.files = Dir["lib/**/*.rb", "LICENSE.txt"]
  spec.extra_rdoc_files = %w[CODE_OF_CONDUCT.md README.md]
  spec.require_paths = ["lib"]

  spec.add_dependency "actionpack", ">= 5.2", "< 6.1.a"

  spec.add_development_dependency "bundler", "~> 2.0"
end
